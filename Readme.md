# Gitlab Pipline templates

This project contains a file with all templates and needed variables.

## Usage
Choose a job to include in deploy stage in order to successfully reach the desired result.

```yaml
stages:
- deploy
```
Add needed variables and files inside the Repo and inside the CICD variables in gitlab.
Create .gitlab-ci.yml file and add to the project with the desired stages included.

## Examples
```yaml
## Deploy Stage
## CD repo must be created.

### Kustomize CD
#### Variables:
    # - GIT_USER : CD repo git user
    # - GIT_USER_MAIL : CD repo git user mail
    # - GIT_ACCESS_TOKEN : CD repo git access token
    # - GIT_CD_REPO_GROUP : CD repo git group
    # - GIT_CD_REPO : CD repo git name
    # - GIT_CD_REPO_BRANCH : CD repo git branch
    # - CONFIG_FILES_LIST : list of files to include in the CD repo
    # - REGISTRY_REPO : registry repository name
    # - REGISTRY_URL : registry url
    # - PROJECT_NAME # PROJECT_NAME=$(echo $CI_PROJECT_NAME | tr '[:upper:]' '[:lower:]')
    # - OP # if [ -z ${OPERATOR+x} ]; then export OP=$OPERATION;else export OP=$OPERATOR/$OPERATION; fi
    # - REGISTRY_PROJECT: $OP/$PROJECT_NAME
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/deployers'
    ref: master
    file: '/kustomize-cd.yml'
### Docker stack CD
#### Variables:
    #   - GIT_CD_REPO
    #   - GIT_CD_REPO_BRANCH
    #   - GIT_USER
    #   - GIT_USER_MAIL
    #   - GIT_ACCESS_TOKEN
    #   - CONFIG_FILES_LIST
    #   - COMPOSE_FILE
    #   - REGISTRY_REPO
    #   - REGISTRY_PROJECT
    #   - REGISTRY_URL
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/deployers'
    ref: master
    file: '/docker-swarm-stack-cd.yml'
### Docker stack deploy
#### Variables:
    # - STACK_NAME
    # - REGISTRY_URL
    # - REGISTRY_USERNAME
    # - REGISTRY_PASSWORD
#### Artifacts None
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/deployers'
    ref: master
    file: '/docker-swarm-stack-deploy.yml'
```